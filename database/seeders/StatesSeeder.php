<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Отправлен запрос',
            ],
            [
                'name' => 'В обработке',
            ],
            [
                'name' => 'Отправлен товар',
            ],
            [
                'name' => 'Выполнен',
            ],
            [
                'name' => 'Отказ',
            ],
        ];

        DB::table('states')->insert($data);
    }
}
