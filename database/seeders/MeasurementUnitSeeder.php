<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MeasurementUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'short_name_kz' => 'г',
                'short_name_ru' => 'г',
                'short_name_en' => 'g',

                'name_kz' => 'грамм',
                'name_ru' => 'грамм',
                'name_en' => 'gram',
            ],
            [
                'short_name_kz' => 'мл',
                'short_name_ru' => 'мл',
                'short_name_en' => 'ml',

                'name_kz' => 'миллилитр',
                'name_ru' => 'миллилитр',
                'name_en' => 'milliliter',
            ],
        ];

        DB::table('measurement_units')->insert($data);
    }
}
