<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'code' => 'AWAITING_CONFIRMATION',
                'name' => 'В ожидании подтверждения',
            ],
            [
                'code' => 'TRANSFERRED_TO_THE_KITCHEN',
                'name' => 'Передан на кухню',
            ],
            [
                'code' => 'PREPARING',
                'name' => 'Готовится',
            ],
            [
                'code' => 'WAITING_FOR_THE_WAITER',
                'name' => 'Ожидает официанта',
            ],
            [
                'code' => 'ORDER_ON_THE_WAY',
                'name' => 'Заказ в пути',
            ],
            [
                'code' => 'AWAITING_PAYMENT',
                'name' => 'Ожидает оплату',
            ],
            [
                'code' => 'CLOSED',
                'name' => 'Закрыт',
            ],
            [
                'code' => 'NOT_PAID',
                'name' => 'Не оплачен',
            ],
            [
                'code' => 'CANCELLED_BY_THE_CUSTOMER',
                'name' => 'Отменен клиентом',
            ],
            [
                'code' => 'CANCELLED',
                'name' => 'Отменен',
            ],
        ];

        DB::table('statuses')->insert($data);
    }
}
