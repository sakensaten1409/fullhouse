<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StoreHousesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'user_id' => 1,
                'address' => "Ивановское Аккольский район, Акмолинская область",
            ],
            [
                'user_id' => 2,
                'address' => "Аккольский лесхоз Аккольский район, Акмолинская область",
            ],
            [
                'user_id' => 3,
                'address' => "Гусарка Аккольский район, Акмолинская область",
            ],
            [
                'user_id' => 4,
                'address' => "Бирлик, Түркістан, Северо-Казахстанская область",
            ],
            [
                'user_id' => 5,
                'address' => "Абай-1, Түркістан, Карагандинская область",
            ],
        ];

        DB::table('store_houses')->insert($data);
    }
}
