<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $t = new StatesSeeder();
        $t->run();
        $t = new UsersSeeder();
        $t->run();
        $t = new StoreHousesSeeder();
        $t->run();
        $t = new ProductsSeeder();
        $t->run();
    }
}
