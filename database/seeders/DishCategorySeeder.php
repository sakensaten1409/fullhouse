<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DishCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name_kz' => 'Ыстық тағамдар',
                'name_ru' => 'Горячие блюда',
                'name_en' => 'Hot dishes',
            ],
            [
                'name_kz' => 'Десерттер',
                'name_ru' => 'Десерты',
                'name_en' => 'Desserts',
            ],
            [
                'name_kz' => 'Сусындар',
                'name_ru' => 'Напитки',
                'name_en' => 'Drinks',
            ],
            [
                'name_kz' => 'Салаттар',
                'name_ru' => 'Салаты',
                'name_en' => 'Salads',
            ],
            [
                'name_kz' => 'Фаст-фуд',
                'name_ru' => 'Фаст-фуд',
                'name_en' => 'Fast food',
            ],
            [
                'name_kz' => 'Тағамдар',
                'name_ru' => 'Закуски',
                'name_en' => 'Snacks',
            ],
        ];

        DB::table('dish_categories')->insert($data);
    }
}
