<?php

namespace App\Http\Requests\Dish;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DishUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:dishes,id',
            'name_kz' => 'string|required',
            'name_ru' => 'string|required',
            'name_en' => 'string|required',
            'price' => 'integer|required',
            'in_stock' => 'boolean|required',
            'dish_category_id' => 'required|exists:dish_categories,id',
            'discount' => 'integer|required',
            'description_kz' => 'string|required',
            'description_ru' => 'string|required',
            'description_en' => 'string|required',
            'photo_link' => 'string|required',
            'visible' => 'boolean|required',
            'measurement_unit_id' => 'required|exists:measurement_units,id',
            'establishment_id' => 'required|exists:establishments,id',
            'preview' => ['required', Rule::when(request()->input('photo_link') === 'changed', ['file'])],

            // optional
            'mass' => 'integer|nullable',
            'calories' => 'integer|nullable',
            'age_limit_value' => 'integer|nullable',
        ];
    }
}
