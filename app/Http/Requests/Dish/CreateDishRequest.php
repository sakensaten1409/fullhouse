<?php

namespace App\Http\Requests\Dish;

use Illuminate\Foundation\Http\FormRequest;

class CreateDishRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_kz' => 'string|required',
            'name_ru' => 'string|required',
            'name_en' => 'string|required',
            'price' => 'integer|required',
            'in_stock' => 'boolean|required',
            'dish_category_id' => 'required|exists:dish_categories,id',
            'discount' => 'integer|required',
            'description_kz' => 'string|required',
            'description_ru' => 'string|required',
            'description_en' => 'string|required',
            'photo_link' => 'nullable',
            'visible' => 'boolean|required',
            'establishment_id' => 'required|exists:establishments,id',

            // optional
            'mass' => 'integer|nullable',
            'measurement_unit_id' => 'exists:measurement_units,id|nullable',
            'calories' => 'integer|nullable',
            'age_limit_value' => 'integer|nullable',
        ];
    }
}
