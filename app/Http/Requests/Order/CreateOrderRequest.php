<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dish_ids' => 'required|array', // todo: надо все id проверить существуют ли они
            'establishment_id' => 'required|exists:establishments,id',
            'client_phone' => 'required|string',
            'table_number' => 'required|int',
            'total_price' => 'required|int',
        ];
    }
}
