<?php

namespace App\Http\Requests\Establishment;

use Illuminate\Foundation\Http\FormRequest;

class EstablishmentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required',
            'main_language_id' => 'required|exists:languages,id',
            'city' => 'nullable',
            'country' => 'nullable',
            'created_at' => 'nullable',
            'id' => 'required|exists:establishments,id',
            'support_phone' => 'nullable',
            'updated_at' => 'nullable',
            'wifi_password' => 'nullable',
            'user_id' => 'required|exists:users,id',
            'banner_photo_link' => 'file',
        ];
    }
}
