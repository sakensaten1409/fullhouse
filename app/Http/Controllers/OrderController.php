<?php

namespace App\Http\Controllers;

use App\Http\Requests\Order\CreateOrderRequest;
use App\Models\Status;
use App\Repositories\Common\OrderDishRelationshipRepository;
use App\Repositories\EstablishmentRepository;
use App\Repositories\OrderRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private OrderRepository $orderRepository;
    private OrderDishRelationshipRepository $ODRRepository;

    public function __construct(OrderRepository $orderRepository, OrderDishRelationshipRepository $orderDishRelationshipRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->ODRRepository = $orderDishRelationshipRepository;
    }

    public function store(CreateOrderRequest $createOrderRequest): JsonResponse
    {
        $orderData = $createOrderRequest->toArray();
        $orderData['to_pay'] = $orderData['total_price'];
        $orderData['status_id'] = Status::AWAITING_CONFIRMATION;
        $newOrderId = $this->orderRepository->store($orderData);

        foreach ($orderData['dish_ids'] as $dishId) {
            $this->ODRRepository->store((int)$newOrderId, (int)$dishId);
        }

        return response()->json([
            'success' => true,
            'id' => $newOrderId
        ]);
    }

    public function getOrderById($id): JsonResponse
    {
        return response()->json([
            'data' => $this->ODRRepository->getById((int)$id)
        ]);
    }

    public function getOrderByOrderId($orderId): JsonResponse
    {
        return response()->json([
            'data' => $this->orderRepository->getOrderByOrderId($orderId)
        ]);
    }

    public function getOrderByIds(Request $request): JsonResponse
    {
        $request->validate([
            'ids' => 'required|array'
        ]);

        return response()->json([
            'data' => $this->orderRepository->getByIds($request->get('ids'))
        ]);
    }

    public function getOrderStatusByOrderIds(Request $request): JsonResponse
    {
        $request->validate([
            'ids' => 'required|array'
        ]);

        return response()->json([
            'data' => $this->orderRepository->getStatusesByOrderIds($request->get('ids'))
        ]);
    }

    public function getOrderPayedMarkByOrderIds(Request $request): JsonResponse
    {
        $request->validate([
            'ids' => 'required|array'
        ]);

        return response()->json([
            'data' => $this->orderRepository->getPayedMarksByOrderIds($request->get('ids'))
        ]);
    }

    public function getOrdersOfEstablishment($estId, EstablishmentRepository $establishmentRepository): JsonResponse
    {
        return response()->json([
            'data' => $establishmentRepository->getOrdersByEstablishmentId((int)$estId)
        ]);
    }

    public function updateStatusOfOrder(Request $request): JsonResponse
    {
        $request->validate([
            'order_id' => 'required|exists:orders,id',
            'status_id' => 'required|exists:statuses,id',
        ]);

        return response()->json([
            'success' => $this->orderRepository->updateStatusById((int)$request->get('order_id'), (int)$request->get('status_id'))
        ]);
    }

    public function updatePayedMarkOfOrder(Request $request): JsonResponse
    {
        $request->validate([
            'order_id' => 'required|exists:orders,id',
            'is_payed' => 'required|boolean',
        ]);

        return response()->json([
            'success' => $this->orderRepository->updatePayedMarkById((int)$request->get('order_id'), (bool)$request->get('is_payed'))
        ]);
    }
}
