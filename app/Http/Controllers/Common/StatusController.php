<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Repositories\Common\StatusesRepository;
use Illuminate\Http\JsonResponse;

class StatusController extends Controller
{
    public function getAllStatusesParams(StatusesRepository $statusesRepository): JsonResponse
    {
        return response()->json([
            'data' => $statusesRepository->getAll()
        ]);
    }
}
