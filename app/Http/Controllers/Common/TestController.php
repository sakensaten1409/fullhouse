<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Mail\TestEmail;
use Illuminate\Support\Facades\Mail;

class TestController extends Controller
{
    public function testEmailSend()
    {
        $emailTo = 'sakensaten1409@gmail.com';
        $firstName = 'Saken';
        $lastName = 'Satenov';

        Mail::to($emailTo)->send(new TestEmail([
            'firstName' => $firstName,
            'lastName' => $lastName,
        ]));
        /*Mail::send(['text' => 'mail'], ['name', 'saken pri'], function ($message) {
            $message->to('sakennekas9@gmail.com', 'to saken')->subject('Test email');
            $message->from('sakennekas9@gmail.com', 'from saken');
        });*/

        return response()->json([
            'test' => 'test response'
        ]);
    }
}
