<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Repositories\Common\MeasurementUnitRepository;

class MeasurementUnitsController extends Controller
{
    public function index(MeasurementUnitRepository $measurementUnitRepository): array
    {
        $columns = ['name_ru', 'id'];
        $data = $measurementUnitRepository->getAll($columns);
        $resArr = [];
        foreach ($data as $item) {
            $resArr[] = [
                'id' => $item->id,
                'short_name' => $item->name_ru,
            ];
        }
        return $resArr;
    }

    public function getAll(MeasurementUnitRepository $measurementUnitRepository)
    {
        $columns = ['*'];
        return $measurementUnitRepository->getAll($columns);
    }
}
