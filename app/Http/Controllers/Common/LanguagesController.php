<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Repositories\Common\LanguageRepository;

class LanguagesController extends Controller
{
    public function index(LanguageRepository $languageRepository)
    {
        $columns = ['name', 'id'];
        return $languageRepository->getAll($columns);
    }

    public function indexWithCodes(LanguageRepository $languageRepository)
    {
        $columns = ['name', 'code'];
        return $languageRepository->getAll($columns);
    }

    public function indexAll(LanguageRepository $languageRepository)
    {
        $columns = ['*'];
        return $languageRepository->getAll($columns);
    }
}
