<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Repositories\Common\RolesRepository;

class RolesController extends Controller
{
    public function index(RolesRepository $rolesRepository)
    {
        $columns = ['name', 'id'];
        $data = $rolesRepository->getAll($columns);
        unset($data[0]); // удаляем user роль
        $rolesTranslateMapping = [
            'user' => 'Владелец ресторана',
            'manager' => 'Менеджер',
            'waiter' => 'Официант',
            'cook' => 'Повар',
        ];
        $rolesArr = [];

        foreach ($data as $role) {
            $rolesArr[] = [
                'id' => $role->id,
                'name' => $rolesTranslateMapping[$role->name]
            ];
        }
        return $rolesArr;
    }
}
