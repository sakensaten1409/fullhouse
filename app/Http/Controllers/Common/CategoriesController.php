<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Repositories\Common\CategoriesRepository;
use Illuminate\Http\JsonResponse;

class CategoriesController extends Controller
{
    public function index(CategoriesRepository $categoriesRepository)
    {
        return $categoriesRepository->getAll();
    }

    public function getCategoryById($id, CategoriesRepository $categoriesRepository): JsonResponse
    {
        return response()->json([
            'data' => $categoriesRepository->getById((int)$id)
        ]);
    }
}
