<?php

namespace App\Http\Controllers;

use App\Repositories\Common\UserEstablishmentRelationshipRepository;
use Illuminate\Http\JsonResponse;

class UserEstablishmentRelationshipController extends Controller
{
    public function getPersonals($estId, UserEstablishmentRelationshipRepository $UERRepository): JsonResponse
    {
        $users = $UERRepository->getPersoanlsByEstablishmentId($estId)->toArray();
        return response()->json([
            'success' => true,
            'data' => $users,
        ]);
    }
}
