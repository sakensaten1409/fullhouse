<?php

namespace App\Http\Controllers;

use App\Http\Requests\Dish\CreateDishRequest;
use App\Http\Requests\Dish\DishUpdateRequest;
use App\Http\Requests\Dish\RemoveDishRequest;
use App\Repositories\DishRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class DishController extends Controller
{
    public function store(CreateDishRequest $createDishRequest, DishRepository $dishRepository): JsonResponse
    {
        $validatedData = $createDishRequest->toArray();
        $validatedData['photo_link'] = config('filesystems.dishes_default_preview_photo_name');
        $validatedData['measurement_unit_id'] = '1';
        if (!is_null($validatedData['age_limit_value']) && (int)$validatedData['age_limit_value'] > 0) {
            $validatedData['age_limit'] = true;
        }

        $id = $dishRepository->store($validatedData);
        if (!empty($id)) {
            return response()->json([
                'success' => true,
                'id' => $id
            ]);
        }

        Log::error('Can not create a dish', [$validatedData, $id]);
        return response()->json([
            'success' => false,
            'message' => 'Can not create a dish',
            'result' => $id
        ], 422);
    }

    public function getDishById($id, DishRepository $dishRepository): JsonResponse
    {
        return response()->json([
            'data' => $dishRepository->getById((int)$id)
        ]);
    }

    public function getDishByIdWithPreview($id, DishRepository $dishRepository): JsonResponse
    {
        $data = $dishRepository->getById((int)$id);
        $originalName = $data['photo_link'];
        $previewPath = "app/public/images/dishes/$id/preview/$originalName";
        if ($originalName === config('filesystems.dishes_default_preview_photo_name')) {
            $previewPath = "app/public/images/dishes/default/preview/$originalName";
        }

        $previewPath = realpath(storage_path($previewPath));
        $mime = mime_content_type($previewPath);
        $preview = "data:$mime;base64," . base64_encode(file_get_contents($previewPath));
        $data['preview'] = $preview;

        return response()->json([
            'data' => $data
        ]);
    }

    public function getAllDishesByEstIdAndCatId($estId, $catId, DishRepository $dishRepository): JsonResponse
    {
        return response()->json([
            'data' => $dishRepository->getAllByEstIdAndCatId((int)$estId, (int)$catId)
        ]);
    }

    public function getAllDishesByEstId($estId, DishRepository $dishRepository): JsonResponse
    {
        return response()->json([
            'data' => $dishRepository->getAllByEstId((int)$estId)
        ]);
    }

    public function getAllDishesByEstIdAndCatIdWithPreview($estId, $catId, DishRepository $dishRepository): JsonResponse
    {
        $data = $dishRepository->getAllByEstIdAndCatId((int)$estId, (int)$catId);

        foreach ($data as &$dish) {
            $originalName = $dish['photo_link'];
            $id = $dish['id'];
            $previewPath = "app/public/images/dishes/$id/preview/$originalName";
            if ($originalName === config('filesystems.dishes_default_preview_photo_name')) {
                $previewPath = "app/public/images/dishes/default/preview/$originalName";
            }

            $previewPath = realpath(storage_path($previewPath));
            $mime = mime_content_type($previewPath);
            $preview = "data:$mime;base64," . base64_encode(file_get_contents($previewPath));
            $dish['preview'] = $preview;
        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function getDishesByArrayOfIds(Request $request, DishRepository $dishRepository): JsonResponse
    {
        $request->validate([
            'ids' => 'required|array'
        ]);
        $dishIds = $request->get('ids');

        $data = $dishRepository->getByIds($dishIds);

        foreach ($data as &$dish) {
            $originalName = $dish['photo_link'];
            $id = $dish['id'];
            $previewPath = "app/public/images/dishes/$id/preview/$originalName";
            if ($originalName === config('filesystems.dishes_default_preview_photo_name')) {
                $previewPath = "app/public/images/dishes/default/preview/$originalName";
            }

            $previewPath = realpath(storage_path($previewPath));
            $mime = mime_content_type($previewPath);
            $preview = "data:$mime;base64," . base64_encode(file_get_contents($previewPath));
            $dish['preview'] = $preview;
        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function update(DishUpdateRequest $request, DishRepository $dishRepository): JsonResponse
    {
        $dishId = $request->get('id');
        $data = $request->toArray();
        if (!is_null($data['age_limit_value']) && (int)$data['age_limit_value'] > 0) {
            $data['age_limit'] = true;
        }
        unset($data['created_at']);
        unset($data['updated_at']);


        $isPreviewSaved = true;
        if ($data['photo_link'] === 'changed') {
            $originalName = $request->file('preview')->getClientOriginalName();
            $path = "public/images/dishes/$dishId/preview/$originalName";
            $isPreviewSaved = Storage::disk('local')->put($path, file_get_contents($request->file('preview')));
            $data['photo_link'] = $originalName;
        }


        unset($data['preview']);


        $res = $dishRepository->updateDishById((int)$dishId, $data);

        if (!$isPreviewSaved) {
            Log::error("Can not save preview image to $path - dishId: $dishId", $request->toArray());
            return response()->json([
                'success' => false,
                'message' => 'Can not save preview image of dish'
            ], 422);
        }

        return response()->json([
            'success' => $res
        ]);
    }

    public function removeDishById(RemoveDishRequest $request, DishRepository $dishRepository)
    {
        return $dishRepository->removeById((int)$request->get('id'));
    }

    public function updateInStockPropByDishId(Request $request, DishRepository $dishRepository): JsonResponse
    {
        $request->validate([
            'dish_id' => 'required|exists:dishes,id',
            'in_stock_value' => 'required|boolean',
        ]);
        $dishId = (int)$request->get('dish_id');
        $inStockProp = (bool)$request->get('in_stock_value');

        return response()->json([
            'success' => $dishRepository->updateInStockPropByDishId($dishId, $inStockProp)
        ]);
    }

    public function updateVisiblePropByDishId(Request $request, DishRepository $dishRepository): JsonResponse
    {
        $request->validate([
            'dish_id' => 'required|exists:dishes,id',
            'visible_value' => 'required|boolean',
        ]);
        $dishId = (int)$request->get('dish_id');
        $visibleProp = (bool)$request->get('visible_value');

        return response()->json([
            'success' => $dishRepository->updateVisiblePropByDishId($dishId, $visibleProp)
        ]);
    }
}
