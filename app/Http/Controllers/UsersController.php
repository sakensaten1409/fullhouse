<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\User;

class UsersController extends Controller
{
    public function getAllUsers()
    {
        return response()->json([
            'data' => User::all()
        ]);
    }
    public function getProductsOfTooById($id)
    {

        return response()->json([
            'data' => Products::query()
            ->where('user_id' , '=', $id)->get()
        ]);
    }
}
