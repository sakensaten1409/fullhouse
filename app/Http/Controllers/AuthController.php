<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Establishment\AddPersonalRequest;
use App\Models\Role;
use App\Repositories\Common\UserEstablishmentRelationshipRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    public function register(RegisterRequest $request, UserRepository $userRepository, Role $role): JsonResponse
    {
        $user = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'role_id' => $role::USER,
        ];

        return response()->json([
            'success' => !empty($userRepository->store($user))
        ]);
    }

    public function login(LoginRequest $request, UserRepository $userRepository): JsonResponse
    {
        $user = $userRepository->getUserByEmail($request->get('email'));

        if (!Hash::check($request->get('password'), $user->password)) {
            return response()->json([
                'errors' => [
                    'password' => ["Invalid password"]
                ],
                'message' => 'The given data was invalid.'
            ], 422);
        }

        return response()->json([
            'token' => $user->createToken('userToken', ['user'])->plainTextToken
        ]);
    }

    public function user(Request $request, UserRepository $userRepository): JsonResponse
    {
        $user = $userRepository->getUserByEmail($request->user()->email);
        return response()->json($user);
    }

    public function addPersonal(AddPersonalRequest $request, UserRepository $userRepository, UserEstablishmentRelationshipRepository $userEstablishmentRelationshipRepository): JsonResponse
    {
        $stuff = $request->toArray();
        $stuffId = $userRepository->store($stuff);
        if (empty($stuffId)) {
            Log::error('Can not add personal', [$stuff]);
            return response()->json([
                'success' => false,
                'message' => 'Can not add personal',
            ], 422);
        }

        $userEstablishmentData = [
            'user_id' => $stuffId,
            'establishment_id' => $stuff['establishment_id'],
        ];

        if (!$userEstablishmentRelationshipRepository->store($userEstablishmentData)) {
            Log::error('Can not add personal and user to user_establishment table', [$stuff]);
            return response()->json([
                'success' => false,
                'message' => 'Can not add personal and user to user_establishment table',
            ], 422);
        }

        return response()->json([
            'success' => true
        ]);
    }
}
