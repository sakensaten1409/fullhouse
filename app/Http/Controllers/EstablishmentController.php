<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddCategoryToEstRequest;
use App\Http\Requests\Establishment\EstablishmentStoreRequest;
use App\Http\Requests\Establishment\EstablishmentUpdateRequest;
use App\Http\Requests\Establishment\RemoveCategoryFromEstRequest;
use App\Models\Establishment;
use App\Repositories\Common\CategoriesRepository;
use App\Repositories\Common\EstablishmentCategoryRelationshipRepository;
use App\Repositories\EstablishmentRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class EstablishmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param EstablishmentRepository $establishmentRepository
     * @return JsonResponse
     */
    public function index(Request $request, EstablishmentRepository $establishmentRepository): JsonResponse
    {
        return response()->json([
            'data' => $establishmentRepository->getAllByUserId((int)$request->user()->id)
        ]);
    }

    public function getEstablishmentsOfPersonal(Request $request, EstablishmentRepository $establishmentRepository): JsonResponse
    {
        return response()->json([
            'data' => $establishmentRepository->getAllByUserIdThroughAdjacent((int)$request->user()->id)
        ]);
    }

    public function getEstablishmentById($id, EstablishmentRepository $establishmentRepository): JsonResponse
    {
        $data = $establishmentRepository->getById((int)$id);
        $originalName = $data['banner_photo_link'];
        $bannerPath = "app/public/images/establishments/$id/banner/$originalName";
        if ($originalName === config('filesystems.default_banner')) {
            $bannerPath = "app/public/images/establishments/default/banner/$originalName";
        }

        $bannerPath = realpath(storage_path($bannerPath));
        $mime = mime_content_type($bannerPath);
        $banner = "data:$mime;base64," . base64_encode(file_get_contents($bannerPath));
        $data['banner'] = $banner;

        return response()->json([
            'data' => $data
        ]);
    }

    public function getEstablishmentByIdWithoutPreview($id, EstablishmentRepository $establishmentRepository): JsonResponse
    {
        $data = $establishmentRepository->getById((int)$id);
        return response()->json([
            'data' => $data
        ]);
    }

    public function getEstablishmentByQrLink($qrLink, EstablishmentRepository $establishmentRepository): JsonResponse
    {
        $data = $establishmentRepository->getByQrLink($qrLink);
        $originalName = $data['banner_photo_link'];
        $id = $data['id'];
        $bannerPath = "app/public/images/establishments/$id/banner/$originalName";
        if ($originalName === config('filesystems.default_banner')) {
            $bannerPath = "app/public/images/establishments/default/banner/$originalName";
        }

        $bannerPath = realpath(storage_path($bannerPath));
        $mime = mime_content_type($bannerPath);
        $banner = "data:$mime;base64," . base64_encode(file_get_contents($bannerPath));
        $data['banner'] = $banner;

        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EstablishmentStoreRequest $request
     * @return JsonResponse
     */
    public function store(EstablishmentStoreRequest $request, EstablishmentRepository $establishmentRepository): JsonResponse
    {
        $data = $request->toArray();
        $data['user_id'] = $request->user()->id;
        $newEstId = $establishmentRepository->store($data);
        if (empty($newEstId) || !$newEstId) {
            Log::error('Can not create an establishment in DB', [$data]);
            return response()->json([
                'success' => false,
                'message' => 'Can not create an establishment in DB',
            ], 422);
        }

        // create QR code and save into storage
        $qrLink = strtolower($data['url']);
        $qrName = $qrLink . '.png';
        $path = "public/images/establishments/$newEstId/qr/$qrName";

        $qrLink = env('QR_CODE_PREFIX') . $qrLink; // https://digitalemenu.kz/menu/show/
        $qrSvg = QrCode::format('png')->size(500)->generate($qrLink);

        $isQrSaved = Storage::disk('local')->put($path, $qrSvg);

        if (!$isQrSaved) {
            Log::error('Can not create the QR code of establishment', [$data, $qrName, $path, $newEstId]);
            return response()->json([
                'success' => false,
                'message' => 'Can not create the QR code of establishment',
            ], 422);
        }
        return response()->json([
            'success' => true
        ]);
    }

    public function testqr(): JsonResponse
    {
        // create QR code and save into storage
        $qrLink = strtolower('qrlinkehe_test_2');
        // https://digitalemenu.kz/menu/show/qrlinkehe
        $qrName = $qrLink . '.png';
        $newEstId = '1';
        $path = "public/images/establishments/$newEstId/qr/$qrName";

        $qrLink = env('QR_CODE_PREFIX') . $qrLink;
        $qrSvg = QrCode::format('png')->size(500)->generate($qrLink);

        $isQrSaved = Storage::disk('local')->put($path, $qrSvg);

//        dd($isQrSaved);

        if (empty($isQrSaved) || !$isQrSaved) {
            Log::error('Can not create the QR code of establishment', [$qrName, $path]);
            return response()->json([
                'success' => false,
                'message' => 'Can not create the QR code of establishment',
            ], 422);
        }
        return response()->json([
            'success' => true
        ]);
    }

    public function getQrCode($estId, $qrLink): JsonResponse
    {
        $bannerPath = "app/public/images/establishments/$estId/qr/$qrLink.png";
        $bannerPath = realpath(storage_path($bannerPath));
        $base64 = "data:image/png;base64," . base64_encode(file_get_contents($bannerPath));
        return response()->json([
            'qr' => $base64
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Establishment $establishment
     * @return Response
     */
    public function show(Establishment $establishment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Establishment $establishment
     * @return Response
     */
    public function edit(Establishment $establishment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EstablishmentUpdateRequest $request
     * @param EstablishmentRepository $establishmentRepository
     * @return JsonResponse
     */
    public function update(EstablishmentUpdateRequest $request, EstablishmentRepository $establishmentRepository): JsonResponse
    {
        $estId = $request->get('id');
        $originalName = $request->file('banner_photo_link')->getClientOriginalName();
        $path = "public/images/establishments/$estId/banner/$originalName";

        $isBannerSaved = Storage::disk('local')->put($path, file_get_contents($request->file('banner_photo_link')));
        $data = [
            'name' => $request->get('name'),
            'country' => $request->get('country') ?? '',
            'city' => $request->get('city') ?? '',
            'support_phone' => $request->get('support_phone') ?? '',
            'wifi_password' => $request->get('wifi_password') ?? '',
            'main_language_id' => $request->get('main_language_id'),
            'banner_photo_link' => $originalName,
        ];
        $res = $establishmentRepository->updateEstablishment((int)$estId, $data);

        if (!$isBannerSaved) {
            Log::error("Can not save banner image to $path - estId: $estId", $request->toArray());
            return response()->json([
                'success' => false,
                'message' => 'Can not save banner image'
            ], 422);
        }

        return response()->json([
            'success' => $res
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Establishment $establishment
     * @return Response
     */
    public function destroy(Establishment $establishment)
    {
        //
    }

    public function addCategoryToEstablishment(AddCategoryToEstRequest $addCategoryToEstRequest, EstablishmentCategoryRelationshipRepository $estCatRelRepo): JsonResponse
    {
        // todo: надо сделать проверку чтобы 2 раза одну и ту же категорию не добавили в один ресторан
        return response()->json([
            'success' => $estCatRelRepo->store($addCategoryToEstRequest->toArray())
        ]);
    }

    public function getEstablishmentCategories($estId, EstablishmentCategoryRelationshipRepository $estCatRelRepo): JsonResponse
    {
        $categories = $estCatRelRepo->getCategoriesByEstablishmentId((int)$estId);
        foreach ($categories as $category) {
            if (empty($category->photo_path)) {
                $photoPath = "app/public/images/categories/default/photo/default_photo.jpg";
                $photoPath = realpath(storage_path($photoPath));
                $category->photo_path = "data:image/jpeg;base64," . base64_encode(file_get_contents($photoPath));
            } else {
                $photoPath = "app/public/images/categories/$category->id/photo/$category->photo_path";
                $photoPath = realpath(storage_path($photoPath));
                $mime = mime_content_type($photoPath);
                $category->photo_path = "data:$mime;base64," . base64_encode(file_get_contents($photoPath));
            }
        }

        return response()->json([
            'establishment_categories' => $categories
        ]);
    }

    public function getEstablishmentCategoriesWithoutPreview($estId, EstablishmentCategoryRelationshipRepository $estCatRelRepo): JsonResponse
    {
        $categories = $estCatRelRepo->getCategoriesByEstablishmentId((int)$estId);

        return response()->json([
            'establishment_categories' => $categories
        ]);
    }

    public function getEstablishmentCategoriesToAdd($estId, EstablishmentCategoryRelationshipRepository $estCatRelRepo, CategoriesRepository $categoriesRepository): JsonResponse
    {
        $allCategories = $categoriesRepository->getAll();

        $categoriesOfEst = $estCatRelRepo->getCategoriesByEstablishmentId((int)$estId);

        $restCategories = [];

        foreach ($allCategories as $allCategory) {
            $exist = false;
            foreach ($categoriesOfEst as $caletoryOfEst) {
                if ($allCategory->id == $caletoryOfEst->id) {
                    $exist = true;
                    break;
                }
            }
            if (!$exist) {
                $restCategories[] = $allCategory;
            }
        }

        return response()->json([
            'establishment_categories_to_add' => $restCategories
        ]);
    }

    public function removeCategoryFromEstablishment(RemoveCategoryFromEstRequest $removeRequest, EstablishmentCategoryRelationshipRepository $estCatRelRepo): JsonResponse
    {
        return response()->json([
            'success' => $estCatRelRepo->removeCategoryFromEstablishment((int)$removeRequest->get('establishment_id'), (int)$removeRequest->get('dish_category_id'))
        ]);
    }

    public function updateName(Request $request, EstablishmentRepository $establishmentRepository): JsonResponse
    {
        $request->validate([
            'name' => 'required|string',
            'establishment_id' => 'required|exists:establishments,id'
            // todo: еще надо проверить, этот ресторан действительно принадлежит к этому юзеру (но это потом)
        ]);

        return response()->json([
            'success' => $establishmentRepository->updateNameById((int)$request->get('establishment_id'), $request->get('name'))
        ]);
    }

    public function updateInfo(Request $request, EstablishmentRepository $establishmentRepository): JsonResponse
    {
        $request->validate([
            'country' => 'required|string',
            'city' => 'required|string',
            'supportPhone' => 'required|string',
            'wifiPassword' => 'required|string',
            'establishmentId' => 'required|exists:establishments,id',
            // todo: еще надо проверить, этот ресторан действительно принадлежит к этому юзеру (но это потом)
        ]);

        return response()->json([
            'success' => $establishmentRepository->updateInfoById(
                $request->get('country'),
                $request->get('city'),
                $request->get('supportPhone'),
                $request->get('wifiPassword'),
                (int)$request->get('establishmentId'),
            )
        ]);
    }

    public function updateMainLanguageByEstId(Request $request, EstablishmentRepository $establishmentRepository): JsonResponse
    {
        $request->validate([
            'establishment_id' => 'required|exists:establishments,id',
            'main_language_id' => 'required|exists:languages,id',
        ]);
        $estId = (int)$request->get('establishment_id');
        $mainLangId = (int)$request->get('main_language_id');

        return response()->json([
            'success' => $establishmentRepository->updateMainLanguageByEstId($estId, $mainLangId)
        ]);
    }
}
