<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFeedbackRequest;
use App\Repositories\FeedbackRepository;
use Illuminate\Http\JsonResponse;

class FeedbackController extends Controller
{
    public function store(CreateFeedbackRequest $createFeedbackRequest, FeedbackRepository $feedbackRepository): JsonResponse
    {
        $id = $feedbackRepository->store($createFeedbackRequest->toArray());

        return response()->json([
            'id' => $id
        ]);
    }

    public function getAllFeedbackByEstablishmentId($estId, FeedbackRepository $feedbackRepository): JsonResponse
    {
        return response()->json([
            'data' => $feedbackRepository->getAllByEstablishmentId((int)$estId)
        ]);
    }
}
