<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'status_id',
        'client_phone',
        'table_number',
        'total_price',
        'to_pay',
        'establishment_id',
        'is_payed',
        'payment_time',
    ];

    public function dishes(): HasMany
    {
        return $this->hasMany(OrderDishRelationship::class)
            ->join('dishes', 'dishes.id', '=', 'order_dish_relationships.dish_id');
    }

    public function dishesName(): HasMany
    {
        return $this->hasMany(OrderDishRelationship::class)
            ->join('dishes', 'dishes.id', '=', 'order_dish_relationships.dish_id')
            ->select('order_dish_relationships.order_id', 'dishes.id', 'dishes.name_ru');
    }

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class);
    }

    public function establishment(): BelongsTo
    {
        return $this->belongsTo(Establishment::class);
    }

    public function feedback(): HasOne
    {
        return $this->hasOne(Feedback::class);
    }
}
