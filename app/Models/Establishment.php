<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Establishment extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name',
        'qr_link',
        'support_phone',
        'main_language_id',
        'country',
        'city',
        'wifi_password',
        'banner_photo_link',
        'rating',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function dishes(): HasMany
    {
        return $this->hasMany(Dish::class);
    }

    public function users_adjacent(): BelongsToMany
    {
        return $this->belongsToMany(UserEstablishmentRelationship::class, 'user_establishment_relationships', 'establishment_id', 'user_id');
    }

    public function categories_adjacent(): HasMany
    {
        return $this->hasMany(EstablishmentCategoryRelationship::class);
    }

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    public function feedbacks(): HasMany
    {
        return $this->hasMany(Feedback::class);
    }
}
