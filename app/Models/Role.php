<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Role extends Model
{
    use HasFactory;

    const USER = 1;
    const MANAGER = 2;
    const WAITER = 3;
    const COOK = 4;

    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }
}
