<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orrders extends Model
{
    use HasFactory;

    protected $fillable = [
        'storehouse_id',
        'count',
        'product_id',
        'to_user_id',
        'from_user_id',
    ];
}
