<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstablishmentCategoryRelationship extends Model
{
    use HasFactory;

    protected $fillable = [
        'establishment_id',
        'dish_category_id',
    ];
}
