<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Dish extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_kz',
        'name_ru',
        'name_en',
        'price',
        'in_stock',
        'dish_category_id',
        'discount',
        'description_kz',
        'description_ru',
        'description_en',
        'photo_link',
        'visible',
        'measurement_unit_id',
        'establishment_id',

        'mass',
        'calories',
        'age_limit_value'
    ];

    public function dish_category(): BelongsTo
    {
        return $this->belongsTo(DishCategories::class);
    }

    public function establishment(): BelongsTo
    {
        return $this->belongsTo(Establishment::class);
    }
}
