<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Status extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'name',
    ];

    const AWAITING_CONFIRMATION = 1;
    const TRANSFERRED_TO_THE_KITCHEN = 2;
    const PREPARING = 3;
    const WAITING_FOR_THE_WAITER = 4;
    const ORDER_ON_THE_WAY = 5;
    const AWAITING_PAYMENT = 6;
    const CLOSED = 7;
    const NOT_PAID = 8;
    const CANCELLED_BY_THE_CUSTOMER = 9;
    const CANCELLED = 10;

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }
}
