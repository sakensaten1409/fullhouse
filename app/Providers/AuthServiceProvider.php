<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Sanctum\PersonalAccessToken;
use Laravel\Sanctum\Sanctum;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Sanctum::authenticateAccessTokensUsing(
            static function (PersonalAccessToken $accessToken) {
                $lastUsedAt = $accessToken->last_used_at ? $accessToken->last_used_at : $accessToken->created_at;

                $sessionLifetime = config('sanctum.expiration');

                if ($lastUsedAt->gt(now()->subMinutes($sessionLifetime))) {
                    return true;
                } else {
                    return false;
                }
            }
        );
    }
}
