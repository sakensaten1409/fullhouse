<?php

namespace App\Repositories\Common;

use App\Models\Role;

class RolesRepository
{
    public function getAll(array $columns)
    {
        return Role::query()->select($columns)->get();
    }
}
