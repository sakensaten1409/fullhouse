<?php

namespace App\Repositories\Common;

use App\Models\Status;

class StatusesRepository
{
    public function getAll()
    {
        return Status::all();
    }
}
