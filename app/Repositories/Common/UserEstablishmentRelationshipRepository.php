<?php

namespace App\Repositories\Common;

use App\Models\User;
use App\Models\UserEstablishmentRelationship;

class UserEstablishmentRelationshipRepository
{
    public function store(array $data)
    {
        $object = new UserEstablishmentRelationship([
            'user_id' => $data['user_id'],
            'establishment_id' => $data['establishment_id'],
        ]);

        return $object->save();
    }

    public function getPersoanlsByEstablishmentId(int $estId)
    {
        $userIds = UserEstablishmentRelationship::query()->where('establishment_id', '=', $estId)->select(['user_id'])->get()->toArray();
        return User::query()->whereIn('id', $userIds)->get();
    }
}
