<?php

namespace App\Repositories\Common;

use App\Models\MeasurementUnit;

class MeasurementUnitRepository
{
    public function getAll(array $columns)
    {
        return MeasurementUnit::query()->select($columns)->get();
    }
}
