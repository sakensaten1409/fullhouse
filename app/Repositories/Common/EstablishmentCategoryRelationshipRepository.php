<?php

namespace App\Repositories\Common;

use App\Models\DishCategories;
use App\Models\Establishment;
use App\Models\EstablishmentCategoryRelationship;

class EstablishmentCategoryRelationshipRepository
{
    public function store(array $data): bool
    {
        $object = new EstablishmentCategoryRelationship([
            'establishment_id' => $data['establishment_id'],
            'dish_category_id' => $data['dish_category_id'],
        ]);

        return $object->save();
    }

    public function getCategoriesByEstablishmentId(int $estId)
    {
        $dishCategoryIds = Establishment::query()->findOrFail($estId)->categories_adjacent()->select(['dish_category_id'])->get();
        $dishCategoryIdsArr = [];
        foreach ($dishCategoryIds as $item) {
            $dishCategoryIdsArr[] = $item->dish_category_id;
        }
        return DishCategories::query()->whereIn('id', $dishCategoryIdsArr)->get();
    }

    public function removeCategoryFromEstablishment(int $estId, int $categoryId)
    {
        return EstablishmentCategoryRelationship::query()
            ->where('establishment_id', '=', $estId)
            ->where('dish_category_id', '=', $categoryId)->delete();
    }
}
