<?php

namespace App\Repositories\Common;

use App\Models\Language;

class LanguageRepository
{
    public function getAll(array $columns)
    {
        return Language::query()->select($columns)->get();
    }
}
