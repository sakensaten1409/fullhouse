<?php

namespace App\Repositories\Common;

use App\Models\DishCategories;

class CategoriesRepository
{
    public function getAll()
    {
        return DishCategories::all();
    }
    public function getById(int $categoryId)
    {
        return DishCategories::query()->findOrFail($categoryId);
    }
}
