<?php

namespace App\Repositories\Common;

use App\Models\OrderDishRelationship;

class OrderDishRelationshipRepository
{
    public function store(int $order_id, int $dish_id): bool
    {
        $object = new OrderDishRelationship([
            'order_id' => $order_id,
            'dish_id' => $dish_id,
        ]);

        return $object->save();
    }

    public function getById(int $id)
    {
        return OrderDishRelationship::query()->findOrFail($id);
    }
}
