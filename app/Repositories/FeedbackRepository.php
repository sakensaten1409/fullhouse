<?php

namespace App\Repositories;

use App\Models\Feedback;

class FeedbackRepository
{
    public function store(array $feedback)
    {
        $feedbackObject = new Feedback([
            'user_name' => $feedback['user_name'] ?? null,
            'order_id' => $feedback['order_id'],
            'establishment_id' => $feedback['establishment_id'],
            'rating' => $feedback['rating'],
            'feedback_text' => $feedback['feedback_text'],
        ]);
        $feedbackObject->save();

        return $feedbackObject->id;
    }

    public function getAllByEstablishmentId(int $estId)
    {
        return Feedback::query()->where('establishment_id', '=', $estId)->orderBy('id', 'desc')->get();
    }
}
