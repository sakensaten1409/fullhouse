<?php

namespace App\Repositories;

use App\Models\Dish;

class DishRepository
{
    public function store(array $dishData)
    {
        $dishObject = new Dish($dishData);
        $dishObject->save();

        return $dishObject->id;
    }

    public function getById(int $dishId)
    {
        return Dish::query()->findOrFail($dishId);
    }

    public function getByIds(array $dishIds)
    {
        return Dish::query()->whereIn('id', $dishIds)->get();
    }

    public function getAllByEstIdAndCatId(int $establishmentId, int $categoryId)
    {
        return Dish::query()
            ->where('establishment_id', '=', $establishmentId)
            ->where('dish_category_id', '=', $categoryId)
            ->get();
    }

    public function getAllByEstId(int $establishmentId)
    {
        return Dish::query()
            ->where('establishment_id', '=', $establishmentId)
            ->get();
    }

    public function updateDishById(int $dishId, array $dishData): int
    {
        return Dish::query()->where('id', '=', $dishId)
            ->update($dishData);
    }

    public function removeById(int $dishId)
    {
        return Dish::query()->where('id', '=', $dishId)
            ->delete();
    }

    public function updateInStockPropByDishId(int $dishId, bool $inStock): int
    {
        return Dish::query()->where('id', '=', $dishId)
            ->update([
                'in_stock' => $inStock
            ]);
    }

    public function updateVisiblePropByDishId(int $dishId, bool $visible): int
    {
        return Dish::query()->where('id', '=', $dishId)
            ->update([
                'visible' => $visible
            ]);
    }
}
