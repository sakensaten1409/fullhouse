<?php

namespace App\Repositories;

use App\Models\Order;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class OrderRepository
{
    public function store(array $orderArr)
    {
        $orderObj = new Order([
            'status_id' => $orderArr['status_id'],
            'client_phone' => $orderArr['client_phone'],
            'table_number' => $orderArr['table_number'],
            'total_price' => $orderArr['total_price'],
            'to_pay' => $orderArr['to_pay'],
            'establishment_id' => $orderArr['establishment_id'],
            'is_payed' => false,
        ]);
        $orderObj->save();

        return $orderObj->id;
    }

    public function getByIds(array $ids)
    {
        return Order::with('dishesName')
            ->select([
                'orders.*',
                'statuses.name as status_name',
                DB::raw('IF(COUNT(feedback.id) > 0, 1, 0) as has_feedback'),
            ])
            ->join('statuses', 'orders.status_id', '=', 'statuses.id')
            ->leftJoin('feedback', 'orders.id', '=', 'feedback.order_id')
            ->whereIn('orders.id', $ids)
            ->groupBy('orders.id')
            ->orderBy('orders.id', 'desc')
            ->get();
    }

    public function getStatusesByOrderIds(array $ids): Collection
    {
        return Order::query()
            ->join('statuses', 'statuses.id', '=', 'orders.status_id')
            ->whereIn('orders.id', $ids)
            ->pluck('statuses.name', 'orders.id');
    }

    public function getPayedMarksByOrderIds(array $ids): Collection
    {
        return Order::query()
            ->whereIn('orders.id', $ids)
            ->pluck('orders.is_payed', 'orders.id');
    }

    public function updateStatusById(int $orderId, int $orderStatusId)
    {
        return Order::query()->find($orderId)
            ->update([
                'status_id' => $orderStatusId
            ]);
    }

    public function updatePayedMarkById(int $orderId, bool $orderPayedMark)
    {
        return Order::query()->find($orderId)
            ->update([
                'is_payed' => $orderPayedMark
            ]);
    }

    public function getOrderByOrderId($orderId)
    {
        return Order::query()->findOrFail($orderId);
    }
}
