<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    public function store(array $user)
    {
        $userObject = new User([
            'name' => $user['name'],
            'email' => $user['email'],
            'password' => Hash::make($user['password']),
            'role_id' => $user['role_id'],
        ]);
        $userObject->save();

        return $userObject->id;
    }

    public function getUserByEmail(string $email)
    {
        return User::query()->where('email', '=', $email)->first();
    }
}
