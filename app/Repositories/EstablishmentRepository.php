<?php

namespace App\Repositories;

use App\Models\Establishment;
use App\Models\User;

class EstablishmentRepository
{
    public function store(array $establishment)
    {
        $establishmentObject = new Establishment([
            'user_id' => $establishment['user_id'],
            'name' => $establishment['name'],
            'qr_link' => strtolower($establishment['url']),
            'main_language_id' => $establishment['language_id'],
            'support_phone' => '',
            'country' => '',
            'city' => '',
            'wifi_password' => '',
            'banner_photo_link' => 'default_establishment_banner.jpg',
        ]);
        $establishmentObject->save();

        return $establishmentObject->id;
    }

    public function getAllByUserId(int $userId)
    {
        return User::query()->find($userId)->establishments()->get();
    }

    public function getAllByUserIdThroughAdjacent(int $userId)
    {
        return User::query()->find($userId)->establishments_adjacent()->get();
    }

    public function getById(int $estId)
    {
        return Establishment::query()->findOrFail($estId);
    }

    public function getByQrLink(string $qrLink)
    {
        return Establishment::query()->where('qr_link', '=', $qrLink)->first();
    }

    public function updateEstablishment(int $estId, array $data): int
    {
        return Establishment::query()->where('id', '=', $estId)
            ->update($data);
    }

    public function updateNameById(int $estId, string $name)
    {
        return Establishment::query()->where('id', '=', $estId)
            ->update([
                'name' => $name
            ]);
    }

    public function updateInfoById(string $country, string $city, string $supportPhone, string $wifiPassword, int $establishmentId)
    {
        return Establishment::query()->where('id', '=', $establishmentId)
            ->update([
                'country' => $country,
                'city' => $city,
                'support_phone' => $supportPhone,
                'wifi_password' => $wifiPassword,
            ]);
    }

    public function getOrdersByEstablishmentId(int $estId)
    {
        return Establishment::query()
            ->find($estId)
            ->orders()->with('dishesName')
            ->select('orders.*', 'statuses.name as status_name')
            ->join('statuses', 'orders.status_id', '=', 'statuses.id')
            ->get();
    }

    public function updateRatingByEstablishmentId(int $establishmentId, float $rating): int
    {
        return Establishment::query()->where('id', '=', $establishmentId)
            ->update([
                'rating' => $rating
            ]);
    }

    public function updateMainLanguageByEstId(int $estId, int $mainLangId): int
    {
        return Establishment::query()->where('id', '=', $estId)
            ->update([
                'main_language_id' => $mainLangId
            ]);
    }
}
