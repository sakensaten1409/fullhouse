<?php

namespace App\Observers;

use App\Models\Feedback;
use App\Repositories\EstablishmentRepository;
use Exception;
use Illuminate\Support\Facades\Log;

class FeedbackObserver
{
    private EstablishmentRepository $establishmentRepository;

    /**
     * @param EstablishmentRepository $establishmentRepository
     */
    public function __construct(EstablishmentRepository $establishmentRepository)
    {
        $this->establishmentRepository = $establishmentRepository;
    }

    /**
     * Handle the Feedback "created" event.
     *
     * @param Feedback $feedback
     * @return void
     */
    public function created(Feedback $feedback)
    {
        try {
            $feedbacksOfEstablishment = $this->establishmentRepository->getById($feedback->establishment_id)->feedbacks()
                ->get();
            $feedbacksCount = 0;
            $sumRating = 0;
            $avgRating = 0;
            foreach ($feedbacksOfEstablishment as $item) {
                $sumRating += (int)$item->rating;
                $feedbacksCount++;
            }
            if ($feedbacksCount > 0) {
                $avgRating = $sumRating / $feedbacksCount;
            }
            $avgRating = round($avgRating, 3);
            $this->establishmentRepository->updateRatingByEstablishmentId($feedback->establishment_id, $avgRating);
        } catch (Exception $e) {
            Log::error('FeedbackObserver: Can not update rating of establishment after leaving a feedback', $feedback->toArray());
            Log::error("File: {$e->getFile()}; At: {$e->getLine()};");
        }
    }

    /**
     * Handle the Feedback "updated" event.
     *
     * @param Feedback $feedback
     * @return void
     */
    public function updated(Feedback $feedback)
    {
        //
    }

    /**
     * Handle the Feedback "deleted" event.
     *
     * @param Feedback $feedback
     * @return void
     */
    public function deleted(Feedback $feedback)
    {
        //
    }

    /**
     * Handle the Feedback "restored" event.
     *
     * @param Feedback $feedback
     * @return void
     */
    public function restored(Feedback $feedback)
    {
        //
    }

    /**
     * Handle the Feedback "force deleted" event.
     *
     * @param Feedback $feedback
     * @return void
     */
    public function forceDeleted(Feedback $feedback)
    {
        //
    }
}
