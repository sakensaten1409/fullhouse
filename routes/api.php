<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Common\CategoriesController;
use App\Http\Controllers\Common\LanguagesController;
use App\Http\Controllers\Common\MeasurementUnitsController;
use App\Http\Controllers\Common\RolesController;
use App\Http\Controllers\Common\StatusController;
use App\Http\Controllers\Common\TestController;
use App\Http\Controllers\DishController;
use App\Http\Controllers\EstablishmentController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\UserEstablishmentRelationshipController;
use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('fh')->group(function () {
    Route::get('/get-all-user', [UsersController::class, 'getAllUsers']);
    Route::get('/get-products-of-too-by-id/{id}', [UsersController::class, 'getProductsOfTooById']);
});




Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth:sanctum', 'ability:user'])->group(function () {
    Route::get('/user', [AuthController::class, 'user']);
    Route::post('/add-personal', [AuthController::class, 'addPersonal']);

    Route::prefix('establishment')->group(function () {
        Route::post('/', [EstablishmentController::class, 'store']);
        Route::get('/', [EstablishmentController::class, 'index']);
        Route::post('/update', [EstablishmentController::class, 'update']);
        Route::post('/testqr', [EstablishmentController::class, 'testqr']);
        Route::get('/qr/{estId}/{qrLink}', [EstablishmentController::class, 'getQrCode']);
        Route::post('/add-category', [EstablishmentController::class, 'addCategoryToEstablishment']);
        Route::get('/categories-to-add/{estId}', [EstablishmentController::class, 'getEstablishmentCategoriesToAdd']);
        Route::post('/remove-category', [EstablishmentController::class, 'removeCategoryFromEstablishment']);
        Route::post('/update-name', [EstablishmentController::class, 'updateName']);
        Route::post('/update-info', [EstablishmentController::class, 'updateInfo']);
        Route::get('/{estId}/orders', [OrderController::class, 'getOrdersOfEstablishment']);
        Route::post('/update/main-lang', [EstablishmentController::class, 'updateMainLanguageByEstId']);
    });

    Route::prefix('personal')->group(function () {
        Route::get('/establishment', [EstablishmentController::class, 'getEstablishmentsOfPersonal']);
    });

    Route::prefix('dish')->group(function () {
        Route::post('/', [DishController::class, 'store']);
        Route::post('/update', [DishController::class, 'update']);
        Route::post('/remove', [DishController::class, 'removeDishById']);
        Route::get('/all-by-est-id/{estId}', [DishController::class, 'getAllDishesByEstId']);
        Route::post('/update/in-stock', [DishController::class, 'updateInStockPropByDishId']);
        Route::post('/update/visible', [DishController::class, 'updateVisiblePropByDishId']);
    });

    Route::prefix('order')->group(function () {
        Route::post('/update/status', [OrderController::class, 'updateStatusOfOrder']);
        Route::post('/update/payed', [OrderController::class, 'updatePayedMarkOfOrder']);
    });

    Route::prefix('user-establishment')->group(function () {
        Route::get('/{estId}/get-personals', [UserEstablishmentRelationshipController::class, 'getPersonals']);
    });

    Route::prefix('roles')->group(function () {
        Route::get('/', [RolesController::class, 'index']);
    });

    Route::prefix('categories')->group(function () {
        Route::get('/', [CategoriesController::class, 'index']);
    });

    Route::prefix('statuses')->group(function () {
        Route::get('/', [StatusController::class, 'getAllStatusesParams']);
    });

    Route::prefix('test')->group(function () {
        Route::post('/email/send', [TestController::class, 'testEmailSend']);
    });
});

// client part

Route::prefix('auth')->group(function () {
    Route::post('/register', [AuthController::class, 'register'])->name('register');
    Route::post('/login', [AuthController::class, 'login'])->name('login');
});

Route::prefix('establishment')->group(function () {
    Route::get('/{id}', [EstablishmentController::class, 'getEstablishmentById']);
    Route::get('/without-preview/{id}', [EstablishmentController::class, 'getEstablishmentByIdWithoutPreview']);
    Route::get('/qr-link/{qrLink}', [EstablishmentController::class, 'getEstablishmentByQrLink']);
    Route::get('/categories/{estId}', [EstablishmentController::class, 'getEstablishmentCategories']);
    Route::get('/categories/without-preview/{estId}', [EstablishmentController::class, 'getEstablishmentCategoriesWithoutPreview']);
});

Route::prefix('languages')->group(function () {
    Route::get('/', [LanguagesController::class, 'index']);
    Route::get('/with-codes', [LanguagesController::class, 'indexWithCodes']);
    Route::get('/all', [LanguagesController::class, 'indexAll']);
});

Route::prefix('measurement-unit')->group(function () {
    Route::get('/', [MeasurementUnitsController::class, 'index']);
    Route::get('/all', [MeasurementUnitsController::class, 'getAll']);
});

Route::prefix('category')->group(function () {
    Route::get('/{id}', [CategoriesController::class, 'getCategoryById']);
});

Route::prefix('dish')->group(function () {
    Route::get('/{id}', [DishController::class, 'getDishById']);
    Route::get('/get-with-preview/{id}', [DishController::class, 'getDishByIdWithPreview']);
    Route::get('/all/{estId}/{catId}', [DishController::class, 'getAllDishesByEstIdAndCatId']);
    Route::get('/get-with-preview/all/{estId}/{catId}', [DishController::class, 'getAllDishesByEstIdAndCatIdWithPreview']);
    Route::get('/get-with-preview2/all-by-ids', [DishController::class, 'getDishesByArrayOfIds']);
});

Route::prefix('order')->group(function () {
    Route::post('/', [OrderController::class, 'store']);
    Route::get('/{id}', [OrderController::class, 'getOrderById']);
    Route::get('/get/order/{orderId}', [OrderController::class, 'getOrderByOrderId']);
    Route::get('/get/bulk', [OrderController::class, 'getOrderByIds']);
    Route::get('/bulk/status', [OrderController::class, 'getOrderStatusByOrderIds']);
    Route::get('/bulk/payed', [OrderController::class, 'getOrderPayedMarkByOrderIds']);
});

Route::prefix('feedback')->group(function () {
    Route::post('/', [FeedbackController::class, 'store']);
    Route::get('/get-all-by-est-id/{estId}', [FeedbackController::class, 'getAllFeedbackByEstablishmentId']);
});
