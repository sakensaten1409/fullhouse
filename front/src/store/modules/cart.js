export default {
    namespaced: true,
    state: {
        dishes: [],
        expireDate: null,
    },
    getters: {
        dishes(state) {
            return state.dishes
        },
        expireDate(state) {
            return state.expireDate
        },
    },
    mutations: {
        SET_DISHES(state, value) {
            state.dishes = value
        },
        SET_EXPIRE_DATE(state, value) {
            state.expireDate = value
        },
    },
    actions: {
        setDishes({commit}, dishes) {
            commit('SET_DISHES', dishes)
        },
        setExpireDate({commit}, expireDate) {
            commit('SET_EXPIRE_DATE', expireDate)
        },
    }
}
