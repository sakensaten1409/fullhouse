export default {
    namespaced: true,
    state: {
        locale: process.env.VUE_APP_I18N_LOCALE,
        languageParams: []
    },
    getters: {
        locale(state) {
            return state.locale
        },
        languageParams(state) {
            return state.languageParams
        },
    },
    mutations: {
        SET_LOCALE(state, value) {
            state.locale = value
        },
        SET_LANGUAGE_PARAMS(state, value) {
            state.languageParams = value
        },
    },
    actions: {
        setLocale({commit}, locale) {
            commit('SET_LOCALE', locale)
        },
        setLanguageParams({commit}, languageParams) {
            commit('SET_LANGUAGE_PARAMS', languageParams)
        },
    }
}
