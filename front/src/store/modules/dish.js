export default {
    namespaced: true,
    state: {
        dish: null,
    },
    getters: {
        dish(state) {
            return state.dish
        },
    },
    mutations: {
        SET_DISH(state, value) {
            state.dish = value
        },
    },
    actions: {
        setDish({commit}, dish) {
            commit('SET_DISH', dish)
        },
    }
}
