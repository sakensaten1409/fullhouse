export default {
    namespaced: true,
    state: {
        showDishInfo: {
            mass: '',
            measurementId: '',
            calories: '',
            ageLimit: '',
            ageLimitValue: '',
        },
        leaveFeedback: {
            establishmentId: '',
            orderId: '',
        }
    },
    getters: {
        showDishInfoGetMass(state) {
            return state.showDishInfo.mass
        },
        showDishInfoGetMeasurementId(state) {
            return state.showDishInfo.measurementId
        },
        showDishInfoGetCalories(state) {
            return state.showDishInfo.calories
        },
        showDishInfoGetAgeLimit(state) {
            return state.showDishInfo.ageLimit
        },
        showDishInfoGetAgeLimitValue(state) {
            return state.showDishInfo.ageLimitValue
        },

        leaveFeedbackGetEstablishmentId(state) {
            return state.leaveFeedback.establishmentId
        },
        leaveFeedbackGetOrderId(state) {
            return state.leaveFeedback.orderId
        },
    },
    mutations: {
        SET_SHOW_DISH_INFO_MASS(state, value) {
            state.showDishInfo.mass = value
        },
        SET_SHOW_DISH_INFO_MEASUREMENT_ID(state, value) {
            state.showDishInfo.measurementId = value
        },
        SET_SHOW_DISH_INFO_CALORIES(state, value) {
            state.showDishInfo.calories = value
        },
        SET_SHOW_DISH_INFO_AGE_LIMIT(state, value) {
            state.showDishInfo.ageLimit = value
        },
        SET_SHOW_DISH_INFO_AGE_LIMIT_VALUE(state, value) {
            state.showDishInfo.ageLimitValue = value
        },

        SET_FEEDBACK_ESTABLISHMENT_ID(state, value) {
            state.leaveFeedback.establishmentId = value
        },
        SET_FEEDBACK_ORDER_ID(state, value) {
            state.leaveFeedback.orderId = value
        },
    },
    actions: {
        setShowDishInfoMass({commit}, mass) {
            commit('SET_SHOW_DISH_INFO_MASS', mass)
        },
        setShowDishInfoMeasurementId({commit}, measurementId) {
            commit('SET_SHOW_DISH_INFO_MEASUREMENT_ID', measurementId)
        },
        setShowDishInfoCalories({commit}, calories) {
            commit('SET_SHOW_DISH_INFO_CALORIES', calories)
        },
        setShowDishInfoAgeLimit({commit}, ageLimit) {
            commit('SET_SHOW_DISH_INFO_AGE_LIMIT', ageLimit)
        },
        setShowDishInfoAgeLimitValue({commit}, ageLimitValue) {
            commit('SET_SHOW_DISH_INFO_AGE_LIMIT_VALUE', ageLimitValue)
        },

        setFeedbackEstablishmentId({commit}, establishmentId) {
            commit('SET_FEEDBACK_ESTABLISHMENT_ID', establishmentId)
        },
        setFeedbackOrderId({commit}, orderId) {
            commit('SET_FEEDBACK_ORDER_ID', orderId)
        },
    }
}
