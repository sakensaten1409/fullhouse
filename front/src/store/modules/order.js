export default {
    namespaced: true,
    state: {
        orderIds: [],
    },
    getters: {
        orderIds(state) {
            return state.orderIds
        },
    },
    mutations: {
        SET_ORDER_IDS(state, value) {
            state.orderIds = value
        },
        ADD_TO_ORDER_IDS(state, value) {
            state.orderIds.push(value)
        },
    },
    actions: {
        setOrderIds({commit}, orderIds) {
            commit('SET_ORDER_IDS', orderIds)
        },
        addToOrderIds({commit}, orderId) {
            commit('ADD_TO_ORDER_IDS', orderId)
        },
    }
}
