export default {
    namespaced: true,
    state: {
        pagetitle: '',
    },
    getters: {
        pagetitle(state) {
            return state.pagetitle
        },
    },
    mutations: {
        SET_PAGETITLE(state, value) {
            state.pagetitle = value
        },
    },
    actions: {
        setPagetitle({commit}, pagetitle) {
            commit('SET_PAGETITLE', pagetitle)
        },
    }
}
