import axios from 'axios'
import router from "@/router"
import Cookies from 'js-cookie'

export default {
    namespaced: true,
    state: {
        authenticated: false,
        user: null,
        token: null,
        establishments: null
    },
    getters: {
        authenticated(state) {
            return state.authenticated
        },
        user(state) {
            return state.user
        },
        token(state) {
            return state.token
        },
        establishments(state) {
            return state.establishments
        },
    },
    mutations: {
        SET_AUTHENTICATED(state, value) {
            state.authenticated = value != null
        },
        SET_USER(state, value) {
            state.user = value
        },
        SET_TOKEN(state, value) {
            state.token = value
        },
        SET_ESTABLISHMENTS(state, value) {
            state.establishments = value
        },
    },
    actions: {
        login({commit}) {
            return axios.get('/user').then(({data}) => {
                commit('SET_USER', data)
                commit('SET_AUTHENTICATED', true)
                router.push({name: 'profile'})
            }).catch(() => {
                commit('SET_USER', null)
                commit('SET_AUTHENTICATED', null)
                commit('SET_TOKEN', null)
                commit('SET_ESTABLISHMENTS', null)
                Cookies.remove('token')
            })
        },
        logout({commit}) {
            commit('SET_USER', null)
            commit('SET_AUTHENTICATED', null)
            commit('SET_TOKEN', null)
            commit('SET_ESTABLISHMENTS', null)
            Cookies.remove('token')
        },
        setToken({commit}, token) {
            commit('SET_TOKEN', token)
            Cookies.set('token', token, {expires: (1 / 1440) * 30}) // 30 minute
        },
        clear({commit}) {
            commit('SET_USER', null)
            commit('SET_AUTHENTICATED', null)
            commit('SET_TOKEN', null)
            commit('SET_ESTABLISHMENTS', null)
            Cookies.remove('token')
            router.push({name: 'login'})
        },
        setEstablishments({commit}, establishment) {
            commit('SET_ESTABLISHMENTS', establishment)
        },
    }
}
