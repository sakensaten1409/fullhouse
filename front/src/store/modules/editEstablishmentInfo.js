export default {
    namespaced: true,
    state: {
        country: '',
        city: '',
        supportPhone: '',
        wifiPassword: '',
    },
    getters: {
        country(state) {
            return state.country
        },
        city(state) {
            return state.city
        },
        supportPhone(state) {
            return state.supportPhone
        },
        wifiPassword(state) {
            return state.wifiPassword
        },
    },
    mutations: {
        SET_COUNTRY(state, value) {
            state.country = value
        },
        SET_CITY(state, value) {
            state.city = value
        },
        SET_SUPPORT_PHONE(state, value) {
            state.supportPhone = value
        },
        SET_WIFI_PASSWORD(state, value) {
            state.wifiPassword = value
        },
    },
    actions: {
        setCountry({commit}, locale) {
            commit('SET_COUNTRY', locale)
        },
        setCity({commit}, locale) {
            commit('SET_CITY', locale)
        },
        setSupportPhone({commit}, locale) {
            commit('SET_SUPPORT_PHONE', locale)
        },
        setWifiPassword({commit}, locale) {
            commit('SET_WIFI_PASSWORD', locale)
        },
    }
}
