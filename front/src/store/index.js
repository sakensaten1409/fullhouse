import {createStore} from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import auth from "./modules/auth";
import client from "./modules/client";
import editEstablishmentInfo from "@/store/modules/editEstablishmentInfo";
import dish from "@/store/modules/dish";
import cart from "@/store/modules/cart";
import order from "@/store/modules/order";
import modals from "@/store/modules/modals";
import pagetitle from "@/store/modules/pagetitle";

export default createStore({
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    modules: {
        auth,
        client,
        editEstablishmentInfo,
        dish,
        cart,
        order,
        modals,
        pagetitle,
    },
    plugins: [
        createPersistedState()
    ]
})
