import 'bootstrap/dist/css/bootstrap.css'
import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import i18n from "@/plugins/i18n";
import httpServicePlugin from '@/plugins/axios'
import LoadingPlugin from "vue-loading-overlay";
import 'vue-loading-overlay/dist/css/index.css';
import helpers from "@/plugins/helpers";
import { createMetaManager } from "vue-meta";
import { plugin as vueMetaPlugin } from 'vue-meta'

import AppButton from '@/components/common/AppButton'

const app = createApp(App)

app.use(store)
    .use(i18n)
    .use(router)
    .use(httpServicePlugin)
    .use(helpers)
    .use(createMetaManager())
    .use(vueMetaPlugin)

// Globally register your component
app.component('VueLoader', LoadingPlugin);
app.component('AppButton', AppButton);

app.mount('#app')

import 'bootstrap/dist/js/bootstrap'
