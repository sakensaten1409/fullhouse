export default function canUpdateEstablishment({to, next, store}) {
    let establishments = store.getters['auth/establishments'];
    if (establishments) {
        const estId = to.params.estId;
        for (const establishment of establishments) {
            if (parseInt(establishment.id) === parseInt(estId)) {
                return next();
            }
        }
        return next({
            name: 'profile',
        })
    } else {
        return next({
            name: 'profile',
        })
    }
}
