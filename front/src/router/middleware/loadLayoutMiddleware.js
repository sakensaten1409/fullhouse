/**
 * Эта мидлварка используется для динамического обновления системы Layouts.
 *
 * Как только измениться route пытается из роута вытащить layout который мы хотим отобразить. Потом загружает компонент лейаута, и присваивает загруженный компонент к мете в переменную layoutComponent. А layoutComponent используется в базовым лейауте AppLayout.vue, там динамический обновляет компонент лейаута
 *
 * Если не найден layout который мы хотим отобразить, загружает дефолтный лейаут AppLayoutDefault.vue
 * */
export async function loadLayoutMiddleware(route) {
    try {
        let layout = route.meta.layout
        let layoutComponent = await import(`@/layouts/${layout}.vue`)
        route.meta.layoutComponent = layoutComponent.default
    } catch (e) {
        console.error('Error occurred in processing of layouts: ', e)
        console.log('Mounted default layout AppLayoutDefault')
        let layout = 'AppLayoutDefault'
        let layoutComponent = await import(`@/layouts/${layout}.vue`)
        route.meta.layoutComponent = layoutComponent.default
    }
}
