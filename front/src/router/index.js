import {createRouter, createWebHistory} from 'vue-router'
import {loadLayoutMiddleware} from "@/router/middleware/loadLayoutMiddleware";
import auth from './middleware/auth'
import store from "@/store";
import middlewarePipeline from "@/router/middleware/middlewarePipeline";
import canUpdateEstablishment from "@/router/middleware/canUpdateEstablishment";

const routes = [
    {
        path: '/',
            name: 'welcome',
        component: () => import('@/views/MainPage'),
        meta: {
            layout: 'FullhouseLayout'
        }
    },
    {
        path: '/cart',
        name: 'cart',
        component: () => import('@/views/CartPage'),
        meta: {
            layout: 'FullhouseTitleLayout'
        }
    },
    {
        path: '/store',
        name: 'store',
        component: () => import('@/views/ListOfStoreHousePage.vue'),
        meta: {
            layout: 'FullhouseTitleLayout'
        }
    },
    {
        path: '/request',
        name: 'request',
        component: () => import('@/views/RequestPage.vue'),
        meta: {
            layout: 'FullhouseTitleLayout'
        }
    },
    {
        path: '/request/requestDetail',
        name: 'requestDetail',
        component: () => import('@/views/DetailRequestPage.vue'),
        meta: {
            layout: 'FullhouseTitleLayout'
        }
    },
    {
        path: '/store/detail',
        name: 'storeDetail',
        component: () => import('@/views/StoreHousePage.vue'),
        meta: {
            layout: 'FullhouseTitleLayout'
        }
    },
    {
        path: '/history',
        name: 'historyPage',
        component: () => import('@/views/HistoryPage'),
        meta: {
            layout: 'FullhouseTitleLayout'
        }
    },
    {
        path: '/history/detail',
        name: 'detailHistoryPage',
        component: () => import('@/views/DetailHistoryPage'),
        meta: {
            layout: 'FullhouseTitleLayout'
        }
    },
    {
        path: '/cart/status/page',
        name: 'statusPage',
        component: () => import('@/views/PaymentStatusPage.vue'),
        meta: {
            layout: 'FullhouseEmptyLayout'
        }
    },
    {
        path: '/welcome/toos/page/:id',
        name: 'toosPage',
        component: () => import('@/views/ToosProductsPage'),
        meta: {
            layout: 'FullhouseLayout'
        }
    },
    {
        path: '/welcome/toos/page/detail',
        name: 'detailProductPage',
        component: () => import('@/views/DetailProductPage'),
        meta: {
            layout: 'FullhouseEmptyLayout'
        }
    },
    {
        path: '/register',
        name: 'register',
        component: () => import('@/views/authViews/RegisterView'),
        meta: {
            layout: 'AppLayoutGuest',
        }
    },
    {
        path: '/register',
        name: 'register',
        component: () => import('@/views/authViews/RegisterView'),
        meta: {
            layout: 'AppLayoutGuest',
        }
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('@/views/authViews/LoginView'),
        meta: {
            layout: 'AppLayoutGuest',
        }
    },
    {
        path: '/terms-of-service',
        name: 'termsOfServicePage',
        component: () => import('@/views/common/TermsOfServicePage'),
        meta: {
            layout: 'AppLayoutGuest',
        }
    },
    {
        path: '/contacts',
        name: 'contacts',
        component: () => import('@/views/common/ContactsPage'),
        meta: {
            layout: 'AppLayoutGuest',
        }
    },
    {
        path: '/profile',
        name: 'profile',
        component: () => import('@/views/personal/ProfilePage'),
        meta: {
            middleware: [
                auth,
            ],
            layout: 'AppLayoutUser',
        }
    },
    {
        path: '/menu',
        name: 'menu',
        render: (c) => c('router-view'),
        children: [
            {
                path: 'show/:qrLink',
                name: 'menuCategories',
                component: () => import('@/views/client/MenuPage'),
                meta: {
                    layout: 'AppLayoutMenu',
                },
            },
            {
                path: 'show/:qrLink/:catId',
                name: 'menuDishes',
                component: () => import('@/views/client/MenuFoodPage'),
                meta: {
                    layout: 'AppLayoutMenu',
                },
            },
            {
                path: 'order/:qrLink',
                name: 'order',
                component: () => import('@/views/client/OrderPage'),
                meta: {
                    layout: 'AppLayoutMenu',
                },
            },
            {
                path: 'settings/:estId',
                name: 'settings',
                component: () => import('@/views/personal/SettingsPage'),
                props: true,
                meta: {
                    middleware: [
                        auth,
                        canUpdateEstablishment
                    ],
                    layout: 'AppLayoutUser',
                }
            },
            {
                path: 'manage-menu/:estId',
                name: 'manageMenu',
                component: () => import('@/views/personal/MenuManagementPage'),
                props: true,
                meta: {
                    middleware: [
                        auth,
                        canUpdateEstablishment
                    ],
                    layout: 'AppLayoutUser',
                }
            },
            {
                path: 'locales/:estId',
                name: 'locales',
                component: () => import('@/views/personal/LocalesPage'),
                props: true,
                meta: {
                    middleware: [
                        auth,
                        canUpdateEstablishment
                    ],
                    layout: 'AppLayoutUser',
                }
            },
        ],
    },
    {
        path: '/orders/:estId',
        name: 'orders',
        component: () => import('@/views/personal/OrdersPage'),
        meta: {
            middleware: [
                auth,
                canUpdateEstablishment
            ],
            layout: 'AppLayoutUser',
        }
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

router.beforeEach(loadLayoutMiddleware)
router.beforeEach((to, from, next) => {
    if (!to.meta.middleware) {
        return next()
    }
    const middleware = to.meta.middleware
    const context = {
        to,
        from,
        next,
        store
    }
    return middleware[0]({
        ...context,
        next: middlewarePipeline(context, middleware, 1)
    })
})

export default router
