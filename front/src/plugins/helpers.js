import dateHelper from '@/helpers/date-helpers'
import constants from "@/helpers/constants"
import user from "@/helpers/user";

export default {
    install: (app) => {
        app.config.globalProperties.$dateHelper = dateHelper
        app.config.globalProperties.$constants = constants
        app.config.globalProperties.$user = user
    }
}
