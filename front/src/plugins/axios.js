import axios from "axios";
import store from "@/store"

const httpServicePlugin = {
    install(app) {
        axios.defaults.withCredentials = true
        axios.defaults.baseURL = process.env.VUE_APP_API_URL
        axios.defaults.headers['X-Requested-With'] = 'XMLHttpRequest'
        axios.defaults.headers['Accept'] = 'application/json'
        axios.defaults.headers['Content-Type'] = 'application/json'
        axios.interceptors.request.use(request => {
            const token = store.getters['auth/token']

            if (token) {
                request.headers.Authorization = `Bearer ${token}`
            }

            if (['post', 'put', 'delete'].includes(request.method)) {
                return setCSRFToken().then(() => request)
            }

            return request
        })

        const setCSRFToken = () => {
            return axios.get('/sanctum/csrf-cookie');
        }

        axios.interceptors.response.use(
            response => {
                return response
            },
            error => {
                switch (error.response.status) {
                    case 401: // Not logged in
                        store.dispatch('auth/clear')
                        break
                    case 419: // Session expired
                    case 403: // Forbidden
                        store.dispatch("auth/logout")
                        break
                    default:
                        // Allow individual requests to handle other errors
                        return Promise.reject(error)
                }

                return Promise.reject(error)
            }
        )
        app.config.globalProperties.$axios = axios
    }
}

export default httpServicePlugin

