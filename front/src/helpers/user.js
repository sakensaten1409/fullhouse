import store from '@/store/index.js';
import constants from "@/helpers/constants";

const isUser = () => {
    return parseInt(store.getters['auth/user'].role_id) === constants.userRoles.user
}

const isManager = () => {
    return parseInt(store.getters['auth/user'].role_id) === constants.userRoles.manager
}

const isWaiter = () => {
    return parseInt(store.getters['auth/user'].role_id) === constants.userRoles.waiter
}

const isCook = () => {
    return parseInt(store.getters['auth/user'].role_id) === constants.userRoles.cook
}

export default {
    isUser,
    isManager,
    isWaiter,
    isCook,
}
