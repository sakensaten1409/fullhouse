export default Object.freeze({
    userRoles: {
        user: 1,
        manager: 2,
        waiter: 3,
        cook: 4,
    },
    order: {
        statuses: {
            AWAITING_CONFIRMATION: 1,
            TRANSFERRED_TO_THE_KITCHEN: 2,
            PREPARING: 3,
            WAITING_FOR_THE_WAITER: 4,
            ORDER_ON_THE_WAY: 5,
            AWAITING_PAYMENT: 6,
            CLOSED: 7,
            NOT_PAID: 8,
            CANCELLED_BY_THE_CUSTOMER: 9,
            CANCELLED: 10,
        }
    },
    server: {
        domain: 'digitalemenu.kz'
    }
})
