const {defineConfig} = require('@vue/cli-service')
module.exports = defineConfig({
    transpileDependencies: true,
    devServer: {
        allowedHosts: [
            '.e-menu',
        ]
    },
    css: {
        loaderOptions: {
            scss: {
                additionalData: `@import "@/assets/scss/app.scss";`
            }
        }
    },
    chainWebpack: config => {
        config
            .plugin('html')
            .tap(args => {
                args[0].title = "Emenu";
                return args;
            })
    }
})
